from Script import GetLatestURL
from twilio.rest import Client
import CountFile
import Config #Personal Config file

def SendText(URL):
    accountSID = Config.ACCOUNT_SID
    authToken = Config.AUTH_TOKEN
    twilioCli = Client(accountSID, authToken)

    myTwilioNumber = Config.TWILIO_PHONE_NUMBER
    myCellPhone = Config.PERSONAL_PHONE_NUMBER

    try:
        message = twilioCli.messages.create(body=URL, from_=myTwilioNumber, to=myCellPhone)
    except TypeError as error:
        print(error)

def Main():
    print("File Count: " + str(CountFile.FileRead()))
    urlList = GetLatestURL(CountFile.FileRead(), Config.USER_NAME, Config.PASSWORD)
    print("Number of URLs " + str(len(urlList)))

    if len(urlList) == 0:
        print("No new URLs")

    elif len(urlList) < 5:
        for url in urlList:
            print("Sending URL: " + str(url))
            SendText(url)
    else:
        print("List: " + str(urlList))
        SendText("ERROR: MORE THAN 5 URLS")
        CountFile.FileCreate(5555555555)  # Safeguard to prevent sending new texts all the time

Main()